import './style.scss';
import 'bootstrap';
//console.log('grace');

let $bouton = $('#box'),
    //console.log($bouton);
    $text = $('#input'),
    //console.log($text);
    $recherche = $('#btn'),
    //console.log($recherche);
    $footer = $('#footer'),
    //console.log($footer);
    $action = $('#action'),
    //console.log($action);
    $romantique = $('#romantique'),
    //console.log($romantique);
    $drame = $('#drame'),
    //console.log($drame);
    $comique = $('#comique'),
    //console.log($comique);
    $jeunesse = $('#jeunesse'),
    //console.log($jeunesse);
    $social = $('#social'),
    //console.log($social);
    $celebrite = $('#btn'),
    $serie = $('#btn1'),
    $formulaire = $('#form');

$(function () {

    $formulaire.on('submit', function (event) {
        console.log($recherche);
        // On évite le rechargement de page
        event.preventDefault();

        let textValue = $text.val();
        if (!textValue) {
            return;
        }
        $('#contenair').html('');
        $.get(`https://api.themoviedb.org/3/search/movie?api_key=0c93d4390e145ebe92f3ccaa26c2514d&language=en-US&query=${textValue}&page=1&include_adult=false`, function (response) {
            console.log(response.results[0].title);
            $('#container').html('');
                if (!response) {
                alert('Introuvable')
            } else {
                for (const resultat of response.results) {
                    //console.log(resultat)
                    //création de variable à recuperer
                    let title = resultat.title,
                        id = resultat.id,
                        popularity = resultat.popularity,
                        vote_count = resultat.vote_count,
                        vote_average = resultat.vote_average,
                        release_date = resultat.release_date,
                        original_language = resultat.original_language,
                        original_title = resultat.original_title,
                        genre_ids = resultat.genre_ids,
                        video = resultat.video,
                        backdrop_path = resultat.backdrop_path,
                        poster_path = resultat.poster_path,
                        overview = resultat.overview;


                    //création de template pour faire apparaitre mes variable
                    let template = `
    <div class="film col-md-12">
        <div class="card mb-3">  
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="https://image.tmdb.org/t/p/w300${poster_path}" class="card-img" alt="${title}">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h2 class="card-title">Titre : ${title}</h2>
                        <p class="card-title">Date de diffusion: ${release_date}</p> 
                        <h3 class="card-title">Titre-original: ${original_title}</h3> 
                        <p class="card-text">Resumé : ${overview.slice(0, 500)}</p> 

                        
                    </div>
                </div>
            </div>
        </div>
    </div>`;



                    $('#container').append(template);
                };

            }

        });

    });

})


$(function celebrite() {
    $formulaire.on('submit', function (event) {
        event.preventDefault();
        let textValue = $text.val();
        if (!textValue) {
            return;
        }

        $('#contenair').html('');
        $.get(`https://api.themoviedb.org/3/person/${textValue}?api_key=<<api_key>>&language=en-US`, function (response) {
            $('#contenair').html('');

            if (!response) {
                alert('Introuvable')
            } else {
                for (const resultat of response.results) {
                    //console.log(resultat)
                    //création de variable à recuperer
                    let title = resultat.title,
                        id = resultat.id,
                        popularity = resultat.popularity,
                        vote_count = resultat.vote_count,
                        vote_average = resultat.vote_average,
                        release_date = resultat.release_date,
                        original_language = resultat.original_language,
                        original_title = resultat.original_title,
                        genre_ids = resultat.genre_ids,
                        video = resultat.video,
                        backdrop_path = resultat.backdrop_path,
                        poster_path = resultat.poster_path,
                        overview = resultat.overview;


                    //création de template pour faire apparaitre mes variable
                    let template = `
    <div class="film col-md-12">
        <div class="card mb-3">  
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="https://image.tmdb.org/t/p/w300${poster_path}" class="card-img" alt="${title}">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h2 class="card-title">Titre : ${title}</h2>
                        <p class="card-title">Date de diffusion: ${release_date}</p> 
                        <h3 class="card-title">Titre-original: ${original_title}</h3> 
                        <p class="card-text">Resumé : ${overview.slice(0, 500)}</p> 

                        
                    </div>
                </div>
            </div>
        </div>
    </div>`;



                    $('#container').append(template);
                

            }

        }

        })
})
})
